from tensorflow import keras
import tensorflow as tf
import numpy as np
import os


model = keras.models.load_model('MLmodel')
class_names = ['balkon', 'inne', 'kuchnia', 'lazienka', 'planMieszkania', 'przedpokoj', 'salon', 'sypialnia', 'zewnetrze']

imageFiles = os.listdir('toClassify')

for imageFile in imageFiles:

    img = keras.preprocessing.image.load_img(
        'toClassify\\'+imageFile, target_size=(184, 184)
    )

    img_array = keras.preprocessing.image.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0) # Create a batch

    predictions = model.predict(img_array)
    score = tf.nn.softmax(predictions[0])

    print(
        "{} image most likely belongs to {} with a {:.2f} percent confidence."
        .format(imageFile, class_names[np.argmax(score)], 100 * np.max(score))
    )

    os.rename('toClassify\\'+imageFile,'classified\\'+class_names[np.argmax(score)]+'\\'+imageFile)