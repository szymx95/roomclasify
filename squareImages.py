import multiprocessing
import os
from PIL import Image, ImageOps
def squareImage(imageQueue):
    while not imageQueue.empty():
        try:
            imgFile = imageQueue.get(timeout=5)
        except:
            return True
        try:
            desired_size = 184
            imgPath = 'otoDomData//' + imgFile
            im = Image.open(imgPath)
            old_size = im.size  # old_size[0] is in (width, height) format

            ratio = float(desired_size)/max(old_size)
            new_size = tuple([int(x*ratio) for x in old_size])
            # use thumbnail() or resize() method to resize the input image

            # thumbnail is a in-place operation

            # im.thumbnail(new_size, Image.ANTIALIAS)

            im = im.resize(new_size, Image.ANTIALIAS)
            # create a new image and paste the resized on it

            new_im = Image.new("RGB", (desired_size, desired_size))
            new_im.paste(im, ((desired_size-new_size[0])//2,
                                (desired_size-new_size[1])//2))

            new_im.save('otoDomImagesSquare//'+imgFile)
        except:
            pass
if __name__=='__main__':
    imagesList = os.listdir('otoDomData')
    
    imageQueue = multiprocessing.Queue()

    
    for e in imagesList:
        imageQueue.put(e)
    #squareImage(imageQueue)
    processes = [multiprocessing.Process(target = squareImage, args = (imageQueue,) ) for e in range(1,12)]

    for e in processes:
        e.start()