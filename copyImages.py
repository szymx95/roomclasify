import os
import shutil


classify = {1:"salon",2:"kuchnia",3:"lazienka",4:"przedpokoj",5:"sypialnia",6:"balkon",7:"zewnetrze",8:"planMieszkania",9:"inne"}

pliki = open('labels.csv','r').readlines()

for e in pliki:
    m = e.strip().split(';')
    
    shutil.copyfile('otoDomImagesSquare\\'+m[0],'mieszkania\\'+classify[int(m[1])]+'\\' + m[0])