import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential
import pathlib
from tensorflow.keras.layers import Flatten, Dense, Dropout


data_dir = pathlib.Path('mieszkania')

image_count = len(list(data_dir.glob('*/*.jpg')))
batch_size = 12
img_height = 184
img_width = 184

val_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="validation",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="training",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)


class_names = train_ds.class_names



AUTOTUNE = tf.data.experimental.AUTOTUNE

val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)




num_classes = 9
params_best = [145, 155, 115, 5, 95, 75, 155, 9]
best_acc = 0
p9 = 9
"""
for e in range(0,7):
  best_acc = 0
  for i in range(5,205,10):


    params = [x for x in params_best]
    params[e] = i
    print('\n\n\n')
    print('Current params :')
    print(params)
    model = Sequential([
      layers.experimental.preprocessing.Rescaling(1./255, input_shape=(img_height, img_width, 3)),
      layers.Conv2D(params[0], 3, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Conv2D(params[1], 3, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Conv2D(params[2], 3, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Conv2D(params[3], 3, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Conv2D(params[4], 3, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Conv2D(params[5], 3, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Flatten(),
      layers.Dense(params[6], activation='relu'),
      layers.Dense(params[7]),
    ])

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

    epochs=10
    history = model.fit(
      train_ds,
      validation_data=val_ds,
      epochs=epochs,
      verbose=1
    )
    if history.history['val_accuracy'][-1] > best_acc:
      best_acc = history.history['val_accuracy'][-1]
      params_best[e] = i
    print("best acc at %f" % (best_acc))
    print(params_best)
"""
model = Sequential([
  layers.experimental.preprocessing.Rescaling(1./255, input_shape=(img_height, img_width, 3)),
      layers.Conv2D(8, 4, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Conv2D(16, 5, padding='same', activation='relu'),
      layers.MaxPooling2D(),
    
      layers.Flatten(),
      layers.Dense(32, activation='relu'),
      layers.Dense(9),
])

model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

epochs=10
history = model.fit(
  train_ds,
  validation_data=val_ds,
  epochs=epochs
)
print(history.history['val_accuracy'][-1])
model.save('MLmodel')

sunflower_path = "otoDomImagesSquare\\51554564_5.jpg"


img = keras.preprocessing.image.load_img(
    sunflower_path, target_size=(img_height, img_width)
)

img_array = keras.preprocessing.image.img_to_array(img)
img_array = tf.expand_dims(img_array, 0) # Create a batch

predictions = model.predict(img_array)
score = tf.nn.softmax(predictions[0])

print(
    "This image most likely belongs to {} with a {:.2f} percent confidence."
    .format(class_names[np.argmax(score)], 100 * np.max(score))
)